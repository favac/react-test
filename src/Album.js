import React, { Component } from 'react'
import { Row, Col } from 'reactstrap'
import Photo from './Photo'

export default ({ items, borderColor }) => {
  return (
    <div>
      <Row style={{ marginBottom: 20 }}>
        <Col md="3" />
        <Col md="3">
          <Photo item={items[0]} borderColor={borderColor} />
        </Col>
        <Col md="3">
          <Photo item={items[1]} borderColor={borderColor} />
        </Col>
        <Col md="3" />
      </Row>
    </div>
  )
}
