import React, { Component } from 'react'
import './App.css'
import axios from 'axios'
import Album from './Album'

const border = {
  0: 'green',
  1: 'blue',
  2: 'purple'
}

class App extends Component {
  state = {
    albums: []
  }

  loadData = async () => {
    let result = await axios.get(`https://jsonplaceholder.typicode.com/photos`)
    let allData = result.data

    let ids = []
    //  lets take the albums ids
    allData.forEach(r => {
      if (!ids.includes(r.albumId)) {
        ids.push(r.albumId)
      }
    })

    // the last 3 albums
    let last3 = ids.slice(-3)

    let data = []

    // lets get data for each album
    last3.forEach((aId, index) => {
      data[index] = { albumId: aId, items: [] }
      // al items
      let items = allData.filter(r => {
        return r.albumId === aId
      })
      // the last 2 items
      data[index].items = items.slice(-2)
    })

    this.setState({ albums: data })
  }

  componentDidMount = async () => {
    await this.loadData()
  }

  render() {
    let { albums } = this.state
    return (
      <div className="App">
        <h1>Photo Albums</h1>
        {albums.map((album, index) => {
          return (
            <div>
              <Album
                key={`album_${index}`}
                items={album.items}
                borderColor={border[index]}
              />
            </div>
          )
        })}
      </div>
    )
  }
}

export default App
