import React, { Component } from 'react'
import { Card, CardImg, CardText, CardBody } from 'reactstrap'

export default ({ item, borderColor }) => {
  return (
    <Card style={{ borderWidth: 5, borderColor, borderRadius: 10 }}>
      <CardImg top width="100%" src={item.thumbnailUrl} />
      <CardBody>
        <CardText>{item.title}</CardText>
      </CardBody>
    </Card>
  )
}
